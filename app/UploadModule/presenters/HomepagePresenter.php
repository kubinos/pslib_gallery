<?php

namespace App\UploadModule\Presenters;

use Nette,
	App\UploadModule\Forms\UploadFormFactory;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
	/** @var UploadFormFactory @inject */
	public $uploadFactory;

	protected function createComponentUploadForm()
	{
		$form = $this->uploadFactory->create();
		$form->onSuccess[] = function ($form, $values) {

		};
		return $form;
	}
}
