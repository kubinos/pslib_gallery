<?php

namespace App\UploadModule\Forms;

use App\UploadModule\models\ImageServant;
use Nette,
	Nette\Application\UI\Form;

class UploadFormFactory extends Nette\Object
{

	/** @var FormFactory */
	private $factory;

	/** @var  ImageServant */
	private $imageServant;

	public function __construct(FormFactory $factory, ImageServant $imageServant)
	{
		$this->factory = $factory;
		$this->imageServant = $imageServant;
	}
	
	/**
	 * @return Form
	 */
	public function create()
	{
		$form =  $this->factory->create();
		$form->addUpload('images', '', true)
			->setAttribute('class', 'field-input');

		$form->addSubmit('upload')
			->setAttribute('class', 'awe-btn awe-btn-1 awe-btn-lager');

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{
		foreach ($values->images as $item) {
			$this->imageServant->prepareImage($item->temporaryFile);
		}
	}

}