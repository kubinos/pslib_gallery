<?php

namespace App\UploadModule\Models;

use Nette\Utils\Image;
use Nette\Utils\Random;

class ImageServant
{
	public function prepareImage($temp)
	{
		$name = Random::generate();
		Image::fromFile($temp)->save("images/upload/original/" .$name .".jpg", null, Image::JPEG);
		Image::fromFile($temp)->resize(93, 62, Image::EXACT)->save("images/upload/thumbnail/" .$name .".jpg", 80, Image::JPEG);
		Image::fromFile($temp)->resize(1170, 480, Image::EXACT)->save("images/upload/slider/" .$name .".jpg", 100, Image::JPEG);
	}

}