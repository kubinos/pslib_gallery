<?php

namespace App\FrontModule\Presenters;

use Nette;
use Nette\Utils\Finder;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
	private function searchImages($dir)
	{
		$paths = [];
		foreach (Finder::findFiles('*.jpg')->in($dir) as $key => $file) {
			$paths[] = $file;
		}
		return $paths;
	}

	public function renderDefault()
	{
		$this->template->paths = $this->searchImages('images/upload/slider');
	}
}
